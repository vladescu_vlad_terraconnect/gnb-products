package com.terraconnect.gnbproducts.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.terraconnect.gnbproducts.models.GetData
import com.terraconnect.gnbproducts.models.RateList
import com.terraconnect.gnbproducts.models.TransactionList
import com.terraconnect.gnbproducts.ultils.CovertorMethods
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class MainViewModel : ViewModel() {
    var listTransactionInitial = arrayListOf<TransactionList.TransactionListItem>()

    var lstTransaction = MutableLiveData<ArrayList<TransactionList.TransactionListItem>>()
    var newlistTransaction = arrayListOf<TransactionList.TransactionListItem>()
    var lstRates = MutableLiveData<ArrayList<RateList.RateListItem>>()
    var newlistRates = arrayListOf<RateList.RateListItem>()
    var myCompositeDisposable: CompositeDisposable? = null
    private val BASE_URL = "http://gnb.dev.airtouchmedia.com"

    // better implementation will be to use .mergeWith or .zip to perform teh 2 requests in paralell
    fun loadTransactionsData() {
        //Define the Retrofit request//
        val requestInterface = Retrofit.Builder()
            //Set the API’s base URL//
            .baseUrl(BASE_URL)
            //Specify the converter factory to use for serialization and deserialization//
            .addConverterFactory(GsonConverterFactory.create())
            //Add a call adapter factory to support RxJava return types//
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            //Build the Retrofit instance//
            .build().create(GetData::class.java)
        //Add all RxJava disposables to a CompositeDisposable//
        myCompositeDisposable?.add(requestInterface.getDataTansacions()
            //Send the Observable’s notifications to the main UI thread//
            .observeOn(AndroidSchedulers.mainThread())
            //Subscribe to the Observer away from the main UI thread//
            .subscribeOn(Schedulers.io())
            .subscribe(this::handleTransactionResponse))
    }

    private fun handleTransactionResponse(transactionsList: TransactionList) {
        listTransactionInitial = transactionsList
        newlistTransaction.addAll(CovertorMethods.removeDuplicates(transactionsList.toList()))
        lstTransaction.value=newlistTransaction
    }

    fun loadRatesData() {
        //Define the Retrofit request//
        val requestInterface = Retrofit.Builder()
            //Set the API’s base URL//
            .baseUrl(BASE_URL)
            //Specify the converter factory to use for serialization and deserialization//
            .addConverterFactory(GsonConverterFactory.create())
            //Add a call adapter factory to support RxJava return types//
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            //Build the Retrofit instance//
            .build().create(GetData::class.java)
        //Add all RxJava disposables to a CompositeDisposable//
        myCompositeDisposable?.add(requestInterface.getDataRates()
            //Send the Observable’s notifications to the main UI thread//
            .observeOn(AndroidSchedulers.mainThread())
            //Subscribe to the Observer away from the main UI thread//
            .subscribeOn(Schedulers.io())
            .subscribe(this::handleRatesResponse))
    }

    private fun handleRatesResponse(rateList: RateList) {
        newlistRates.addAll(rateList)
        lstRates.value=newlistRates
    }

    fun openTransactionDetails(transaction: TransactionList.TransactionListItem): List<TransactionList.TransactionListItem> {
       return CovertorMethods.findAllClickedTransactions(listTransactionInitial,transaction)

    }
}