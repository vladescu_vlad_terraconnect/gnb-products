package com.terraconnect.gnbproducts.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class TransactionDetailsViewModelFactory : ViewModelProvider.Factory{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if(modelClass.isAssignableFrom(TransactionDetailViewModel::class.java)){
            return TransactionDetailViewModel() as T
        }
        throw IllegalArgumentException ("UnknownViewModel")
    }

}