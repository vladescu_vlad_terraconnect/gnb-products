import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.terraconnect.gnbproducts.R
import com.terraconnect.gnbproducts.TransactionDetailsActivity
import com.terraconnect.gnbproducts.models.TransactionList
import com.terraconnect.gnbproducts.ultils.CovertorMethods
import com.terraconnect.gnbproducts.viewModel.MainViewModel


class TransactionAdapter(
    val viewModel: MainViewModel,
    val arrayList: ArrayList<TransactionList.TransactionListItem>?,
    val context: Context
): RecyclerView.Adapter<TransactionAdapter.SKUViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int,
    ): TransactionAdapter.SKUViewHolder {
        var root = LayoutInflater.from(parent.context).inflate(R.layout.item_view, parent, false)
        return SKUViewHolder(root)
    }

    override fun onBindViewHolder(holder: TransactionAdapter.SKUViewHolder, position: Int) {
        if (arrayList != null) {
            holder.bindItems(arrayList.get(position))
        }
    }
    override fun getItemCount(): Int {
        if (arrayList != null) {
            if(arrayList.size==0){
                Toast.makeText(context, "List is empty", Toast.LENGTH_LONG).show()
            }else{

            }
        }
        if (arrayList != null) {
            return arrayList.size
        }else
        {
            return 0
        }
    }
    inner class SKUViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(trasaction: TransactionList.TransactionListItem) {
            val title = itemView.findViewById(R.id.title) as TextView
            val clickOnSeeMore  = itemView.findViewById(R.id.clickOnSeeMore) as RelativeLayout
            title.text = trasaction.sku
            clickOnSeeMore.setOnClickListener {
                var initialList: List<TransactionList.TransactionListItem>
                initialList = viewModel.openTransactionDetails(trasaction)
                var  initialListAmmounts = CovertorMethods.retriveAllTransactionsAmmouns(initialList,trasaction)
                var  initialListCurrency = CovertorMethods.retriveAllTransactionsCurrencyes(initialList,trasaction)
                val intent = Intent(context, TransactionDetailsActivity::class.java)
                intent.putStringArrayListExtra("initialListAmmounts", (ArrayList(initialListAmmounts)) )
                intent.putStringArrayListExtra("initialListCurrency", (ArrayList(initialListCurrency)))
                //calculate total ammount
                var total = CovertorMethods.convertAndCalculateTotalAmmountInEuro(
                    initialListAmmounts as List<String>, initialListCurrency as List<String>
                )
                intent.putExtra("total", total)
                context.startActivity(intent)
            }
        }
    }

}
