package com.terraconnect.gnbproducts.ui.main

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.terraconnect.gnbproducts.R
import com.terraconnect.gnbproducts.models.TransactionList
import com.terraconnect.gnbproducts.viewModel.TransactionDetailViewModel

class DetailsTransactionAdapter(val viewModel: TransactionDetailViewModel, val arrayList: ArrayList<TransactionList.TransactionListItem>?, val context: Context): RecyclerView.Adapter<DetailsTransactionAdapter.SKUViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int,
    ): DetailsTransactionAdapter.SKUViewHolder {
        var root = LayoutInflater.from(parent.context).inflate(R.layout.item_view_details,parent,false)
        return SKUViewHolder(root)
    }

    override fun onBindViewHolder(holder: DetailsTransactionAdapter.SKUViewHolder, position: Int) {
        if (arrayList != null) {
            holder.bindItems(arrayList.get(position))
        }
    }
    override fun getItemCount(): Int {
        if (arrayList != null) {
            if(arrayList.size==0){
                Toast.makeText(context,"List is empty", Toast.LENGTH_LONG).show()
            }else{

            }
        }
        if (arrayList != null) {
            return arrayList.size
        }else
        {
            return 0
        }
    }
    inner class SKUViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(trasaction: TransactionList.TransactionListItem) {
            val amount = itemView.findViewById(R.id.amount) as TextView
            val currency = itemView.findViewById(R.id.currency) as TextView
            amount.text = trasaction.amount
            currency.text = trasaction.currency
        }
    }
}