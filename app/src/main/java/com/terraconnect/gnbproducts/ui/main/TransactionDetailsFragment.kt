package com.terraconnect.gnbproducts.ui.main

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.terraconnect.gnbproducts.R
import com.terraconnect.gnbproducts.viewModel.MainViewModel
import com.terraconnect.gnbproducts.viewModel.MainViewModelFactory
import com.terraconnect.gnbproducts.viewModel.TransactionDetailViewModel
import com.terraconnect.gnbproducts.viewModel.TransactionDetailsViewModelFactory
import io.reactivex.Observer
import io.reactivex.disposables.CompositeDisposable

class TransactionDetailsFragment : Fragment() {

    private var viewManager = LinearLayoutManager(getActivity())
    private lateinit var viewModel: TransactionDetailViewModel
    private lateinit var mainrecycler: RecyclerView

    companion object {
        fun newInstance() = TransactionDetailsFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        val v: View = inflater.inflate(R.layout.main_fragment2, container, false)
        mainrecycler = v.findViewById<View>(R.id.recyclerDetails) as RecyclerView
        mainrecycler.setLayoutManager(viewManager)
        val factory = TransactionDetailsViewModelFactory()
        viewModel = ViewModelProviders.of(this, factory).get(TransactionDetailViewModel::class.java)
         observeData()
        viewModel.loadTransactionsData()
        return v
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(TransactionDetailViewModel::class.java)

    }
    override fun onDestroy() {
        super.onDestroy()

    }
    fun observeData(){
      /*  getActivity()?.let {
            viewModel.lstTransaction.observe(it, Observer {
                Log.i("dataTansaction", it.toString())
                mainrecycler.adapter = getActivity()?.let { it1 -> DetailsTransactionAdapter(viewModel, it, it1) }
            })
        }*/

    }
}