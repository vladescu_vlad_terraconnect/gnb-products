package com.terraconnect.gnbproducts.ui.main

import TransactionAdapter
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.terraconnect.gnbproducts.R
import com.terraconnect.gnbproducts.viewModel.MainViewModel
import com.terraconnect.gnbproducts.viewModel.MainViewModelFactory
import io.reactivex.disposables.CompositeDisposable

class MainFragment : Fragment() {
    private var viewManager = LinearLayoutManager(getActivity())
    private lateinit var viewModel: MainViewModel
    private lateinit var mainrecycler: RecyclerView

    companion object {
        fun newInstance() = MainFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        val v: View = inflater.inflate(R.layout.main_fragment, container, false)
        mainrecycler = v.findViewById<View>(R.id.recycler) as RecyclerView
        mainrecycler.setLayoutManager(viewManager)
        val factory = MainViewModelFactory()
        viewModel = ViewModelProviders.of(this, factory).get(MainViewModel::class.java)
        observeData()
        viewModel.myCompositeDisposable = CompositeDisposable()
        viewModel.loadTransactionsData()
        viewModel.loadRatesData()
        return v
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)

    }
    override fun onDestroy() {
        super.onDestroy()
        viewModel.myCompositeDisposable?.clear()

    }
    fun observeData(){
        getActivity()?.let {
            viewModel.lstTransaction.observe(it, Observer {
                Log.i("dataTansaction", it.toString())
                mainrecycler.adapter = getActivity()?.let { it1 -> TransactionAdapter(viewModel, it, it1) }
            })
        }
        getActivity()?.let {
            viewModel.lstRates.observe(it, Observer {
                Log.i("dataRates", it.toString())
               // mainrecycler.adapter = getActivity()?.let { it1 -> TransactionAdapter(viewModel, it, it1) }
            })
        }
    }
}