package com.terraconnect.gnbproducts.ultils

import com.terraconnect.gnbproducts.models.RateList
import com.terraconnect.gnbproducts.models.TransactionList

class CovertorMethods {
    companion object {
        //gets the returned list from server and removes duplicates products
        fun removeDuplicates(transactionsList: List<TransactionList.TransactionListItem>): List<TransactionList.TransactionListItem> {
            var processedList = transactionsList
            val distinct = processedList.distinctBy { it -> it.sku }
            return distinct
        }

        //gets the returned list of rates fron the server, calculates the missing rates and retuns the complete list of rates
        fun calculateMissingRates(transactionsList: List<RateList.RateListItem>): List<RateList.RateListItem> {
            // calculate the 2 missing rates from the data available
            //calculate  CAD - EUR
            //  var ammountUSDEUR=transactionsList.get(i)
            //calculate missing AUD - CAD
            //calculate missing CAD - AUD
            var processedList = transactionsList
            val distinct = processedList.distinctBy { it -> it.rate }
            return distinct
        }

        //gets the returned list from server and removes duplicates products
        fun findAllClickedTransactions(
            transactionsList: List<TransactionList.TransactionListItem>,
            transactions: TransactionList.TransactionListItem
        ): List<TransactionList.TransactionListItem> {
            var processedList = transactionsList
            val distinct = processedList.filter {
                it.sku == transactions.sku
            }
            return distinct
        }
        fun retriveAllTransactionsCurrencyes(transactionsList: List<TransactionList.TransactionListItem>,transaction: TransactionList.TransactionListItem): List<String?> {
            var listOfCurrency  = arrayListOf<String?>()
            for (item in transactionsList) {
                if(transaction.sku.equals(item.sku)){
                    listOfCurrency.add(item.currency)
                }
            }
            return  listOfCurrency
        }
        fun retriveAllTransactionsAmmouns(transactionsList: List<TransactionList.TransactionListItem>,transaction: TransactionList.TransactionListItem): List<String?> {
            var listOfAmmounts  = arrayListOf<String?>()
            for (item in transactionsList) {
                if(transaction.sku.equals(item.sku)){
                    listOfAmmounts.add(item.amount)
                }
            }
            return  listOfAmmounts
        }
        fun convertAndCalculateTotalAmmountInEuro(initialListAmmounts: List<String>,initialListCurrency: List<String> ):Int{
            //stiil have to define here an algorithm to convert all the values and calculate the TOTAL in EUR
            var total = 9999
            for (item in initialListAmmounts) {
                //here string conversion to int and rpont the floating point to nearest val
                val float1: Float? = item.toFloat()
                total = (total + float1!!).toInt()
            }
            return  total;
        }
    }
}