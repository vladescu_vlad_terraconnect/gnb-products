package com.terraconnect.gnbproducts

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.terraconnect.gnbproducts.ui.main.TransactionDetailsFragment

class TransactionDetailsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.transaction_details_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, TransactionDetailsFragment.newInstance())
                .commitNow()
        }
    }
}