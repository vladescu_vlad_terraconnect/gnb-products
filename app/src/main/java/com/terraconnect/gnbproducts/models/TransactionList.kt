package com.terraconnect.gnbproducts.models

class TransactionList : ArrayList<TransactionList.TransactionListItem>(){
    data class TransactionListItem(
        val amount: String,
        val currency: String,
        val sku: String
    )
}