package com.terraconnect.gnbproducts.models

class RateList : ArrayList<RateList.RateListItem>(){
    data class RateListItem(
        val from: String,
        val rate: String,
        val to: String
    )
}