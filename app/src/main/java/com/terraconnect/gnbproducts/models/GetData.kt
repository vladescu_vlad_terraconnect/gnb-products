package com.terraconnect.gnbproducts.models
import io.reactivex.Observable
import retrofit2.http.GET

interface GetData {

    //The get request to retrive the transactions list//
    @GET("/transactions.json")
    fun getDataTansacions() : Observable<TransactionList>

    //The get request to retrive  the rates list//
    @GET("/rates.json")
    fun getDataRates() : Observable<RateList>
}